jQuery(function($) {
	// 根据设备转到对应的页面
	function setDevice() {
		if('ontouchstart' in document) {
			// 触摸屏 大于750用pc 小于750用mo
			if($(window).width() > 750) {
				if($('.container').hasClass('mo')) {
					window.open('../p.' + $('.container').attr('id').substring(1) + '.html', '_self');
				};
			} else {
				if($('.container').hasClass('pc')) {
					window.open('mobile/m.' + $('.container').attr('id').substring(1) + '.html', '_self');
				};
			};
		} else {
			// 非触摸屏
			if($('.container').hasClass('mo')) {
				window.open('../p.' + $('.container').attr('id').substring(1) + '.html', '_self');
			};
		};
	};
	setDevice();
    $(window).on('resize', function() {
    	setDevice();
    });

    // pc font
	function setPcFontSize() {
		var slashsize = ($(window).width() * 10.2) / 1920,
			slashmsize = ($(window).width() * 5.2) / 1920,
			size2 = ($(window).width() * 3.74) / 1920,
			lineheight2 = ($(window).width() * 5.9) / 1920,
			paddingtop2 = ($(window).width() * 4) / 1920,
			paddingbottom2 = ($(window).width() * 3) / 1920,
			size3 = ($(window).width() * 3) / 1920,
			lineheight3 = ($(window).width() * 4.5) / 1920,
			padding3 = ($(window).width() * 3) / 1920,
			subsize = ($(window).width() * 2.34) / 1920,
			sublineheight = ($(window).width() * 3.98) / 1920;
		if($(window).width() > 960) {
			$('.box-title .slash').css({
				'width': slashsize + 'rem'
			});
			$('.box-title .slash-m').css({
				'width': slashmsize + 'rem'
			});
			$('.box-title h2').css({
				'font-size': size2 + 'rem', 
				'line-height': lineheight2 + 'rem',
				'padding': paddingtop2 + 'rem 0 ' + paddingbottom2 + 'rem'
			});
			$('.box-title h2.nopadding').css({
				'padding': '0rem'
			});
			$('.box-title h3').css({
				'font-size': size3 + 'rem', 
				'line-height': lineheight3 + 'rem',
				'padding': padding3 + 'rem 0'
			});
			$('.subtitle').css({
				'font-size': subsize + 'rem',
				'line-height': sublineheight + 'rem'
			});
		} else {
			$('.box-title .slash').css({
				'width': '5.1rem'
			});
			$('.box-title .slash-m').css({
				'width': '2.6rem'
			});
			$('.box-title h2').css({
				'font-size': '1.87rem', 
				'line-height': '2.95rem',
				'padding': '2rem 0 1.5rem'
			});
			$('.box-title h2.nopadding').css({
				'padding': '0rem'
			});
			$('.box-title h3').css({
				'font-size': '1.5rem', 
				'line-height': '2.25rem',
				'padding': '1.5rem 0'
			});
			$('.subtitle').css({
				'font-size': '1.17rem',
				'line-height': '1.99rem'
			});
		};
	};
	if($('.container').hasClass('pc')) {
		setPcFontSize();
		$(window).on('resize', function() {
	    	setPcFontSize();
	    });
	};
	// mo font
	function setMoFontSize() {
		var slashsize = ($(window).width() * 3.12) / 375;
			// h2fontsize = $(window).width() / 375 * 1.96,
			// h2lineheight = $(window).width() / 375 * 3,
			// h2paddingtop = $(window).width() / 375 * 2.8,
			// h2paddingbottom = $(window).width() / 375 * 2.2,
			// subfontsize = $(window).width() / 375 * 1.28,
			// sublineheight = $(window).width() / 375 * 2.4;
		$('.box-title .slash').css({
			'width': slashsize + 'rem'
		});
		// $('.title h2').css({
		// 	'font-size': h2fontsize + 'rem', 
		// 	'line-height': h2lineheight + 'rem',
		// 	'padding': h2paddingtop + 'rem 0 ' + h2paddingbottom + 'rem'
		// });
		// $('.subtitle').css({
		// 	'font-size': subfontsize + 'rem',
		// 	'line-height': sublineheight + 'rem'
		// });
	};
	if($('.container').hasClass('mo')) {
		setMoFontSize();
		$(window).on('resize', function() {
    		setMoFontSize();
	    });
	};
    

    // common function
    $.extend({
    	// 出现在视野中
    	visionAn: function(el) {
    		if(el.offset().top >= $(window).scrollTop() && el.offset().top < ($(window).scrollTop() + $(window).height() * 0.75)) {
    			return true;
    		} else {
    			return false;
    		};
    	},
    	// 淡入效果
    	fadeUpAn: function(el) {
    		if(el.offset().top >= $(window).scrollTop() && el.offset().top < ($(window).scrollTop() + $(window).height() * 0.75)) {
				el.css({'opacity': '1'});
				el.addClass('animated fadeInUp');
			};
    	},
    	// 180 滑动
    	prod180An: function(id, el, route, startn, endn) {
    		if('ontouchstart' in document) {
    			// 触屏设备
    			var startx, starty; // 起始数值
				// 获得角度
				function getAngle(angx, angy) {
			        return Math.atan2(angy, angx) * 180 / Math.PI;
			    };
				//根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
			    function getDirection(startx, starty, endx, endy) {
			        var angx = endx - startx;
			        var angy = endy - starty;
			        var result = 0;
			        //如果滑动距离太短
			        if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
			            return result;
			        };
			        var angle = getAngle(angx, angy);
			        if (angle >= -135 && angle <= -45) {
			            result = 1;
			        } else if (angle > 45 && angle < 135) {
			            result = 2;
			        } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
			            result = 3;
			        } else if (angle >= -45 && angle <= 45) {
			            result = 4;
			        };
			        return result;
			    };

				var slider = document.getElementById(id);
				// 接触屏幕
				slider.addEventListener('touchstart', function(e) {
					startx = e.changedTouches[0].pageX;
			        starty = e.changedTouches[0].pageY;
				}, false);
				// 滑动屏幕
				slider.addEventListener('touchmove', function(e) {
					var endx, endy;
					endx = e.changedTouches[0].pageX;
					endy = e.changedTouches[0].pageY;
					var dir = getDirection(startx, starty, endx, endy);
					if(dir == 3) {
						// 向左滑动
						// console.log('向左移动距离: ' + (startx - endx));
						if(Math.abs(endx - startx)/1.5 > 1) {
							if(el.data('index') < endn) {
								var num = el.data('index') + 1;
								el.data('index', num);
								el.attr('src', route + num + '.png');
							};
						};
					} else if(dir == 4) {
						// 向右滑动
						// console.log('向右移动距离: ' + (endx - startx));
						if(Math.abs(endx - startx)/1.5 > 1) {
							if(el.data('index') > startn) {
								var num = el.data('index') - 1;
								el.data('index', num);
								el.attr('src', route + num + '.png');
							};
						};
					};
					startx = endx;
					starty = endy;
				}, false);
    		} else {
    			// 非触屏设备
    			var startx, starty, // 起始数值
    				drag = 0; // 是否在拖拽 默认没有
				// 获得角度
				function getAngle(angx, angy) {
			        return Math.atan2(angy, angx) * 180 / Math.PI;
			    };
				//根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
			    function getDirection(startx, starty, endx, endy) {
			        var angx = endx - startx;
			        var angy = endy - starty;
			        var result = 0;
			        //如果滑动距离太短
			        if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
			            return result;
			        };
			        var angle = getAngle(angx, angy);
			        if (angle >= -135 && angle <= -45) {
			            result = 1;
			        } else if (angle > 45 && angle < 135) {
			            result = 2;
			        } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
			            result = 3;
			        } else if (angle >= -45 && angle <= 45) {
			            result = 4;
			        };
			        return result;
			    };

			    // 不允许拖拽
			    $('body').on('dragstart', function() {
			    	return false;
			    });

			    el.on('dragstart', function() {
			    	// 接触屏幕
					el.on('mousedown', function(e) {
						drag = 1;
						startx = e.pageX;
				        starty = e.pageY;
					});
					// 滑动屏幕
					el.on('mousemove', function(e) {
						if(drag == 1) {
							var endx, endy;
							endx = e.pageX;
							endy = e.pageY;
							var dir = getDirection(startx, starty, endx, endy);
							if(dir == 3) {
								// 向左滑动
								// console.log('向左移动距离: ' + (startx - endx));
								if(Math.abs(endx - startx)/1.5 > 1) {
									if(el.data('index') < endn) {
										var num = el.data('index') + 1;
										el.data('index', num);
										el.attr('src', route + num + '.png');
									};
								};
							} else if(dir == 4) {
								// 向右滑动
								// console.log('向右移动距离: ' + (endx - startx));
								if(Math.abs(endx - startx)/1.5 > 1) {
									if(el.data('index') > startn) {
										var num = el.data('index') - 1;
										el.data('index', num);
										el.attr('src', route + num + '.png');
									};
								};
							};
							startx = endx;
							starty = endy;
						};
					});
					el.on('mouseup', function(e) {
						drag = 0;
					});
			    });
    		};
    	}
    });
});  