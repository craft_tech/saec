jQuery(function($) {
    // swiper
    var swiper1 = new Swiper('.swiper1', {
		autoplayDisableOnInteraction : false,
		loop: true,
		pagination: '.swiper-pagination1'
	});

    // swiper
    var swiper2 = new Swiper('.swiper2', {
        autoplayDisableOnInteraction : false,
        loop: true,
        pagination: '.swiper-pagination2'
    });

    // 可选城市 需调接口
    var provinceArr = [
        {
            id: 0,
            value: '上海',
            city: ['上海市']
        },
        {
            id: 1,
            value: '浙江',
            city: ['杭州', '嘉兴', '金华', '义乌']
        }
    ];

    // fill province
    if($('.section1 .province .default').siblings().length == 0) {
        var len = provinceArr.length;
        for(var i = 0; i < len; i++) {
            var $option = $('<option data-id="' + provinceArr[i].id + '" value="' + provinceArr[i].value + '">' + provinceArr[i].value + '</option>');
            $('.section1 .province').append($option);
        };
    };
    // fill city
    $('.section1 .province').on('input propertychange', function() {
        if($('.section1 .city .default').siblings().length > 0) {
            $('.section1 .city .default').siblings().remove();
        };
        if($(this).val() != '') {
            var id = $(this).find('option:selected').data('id');
            var len = provinceArr[id].city.length;
            for(var i = 0; i < len; i++) {
                var $option = $('<option value="' + provinceArr[id].city[i] + '">' + provinceArr[id].city[i] + '</option>');
                $('.section1 .city').append($option);
            };
        };
    });

    // 定位
    $('.section1 .btn-pos').on('click', function() {
        if($('.section1 .province').val() == '') {
            alert('请选择省份');
        } else {
            if($('.section1 .city').val() == '') {
                alert('请选择城市');
            } else {
                map.centerAndZoom($('.section1 .city').val(), 12);
            };
        };
    });

    // 百度地图API功能
    var map = new BMap.Map('shopmap');    // 创建Map实例
    map.centerAndZoom('上海', 12);  // 初始化地图,设置中心点坐标和地图级别
    var local = new BMap.LocalSearch(map, {
        renderOptions:{map: map}
    });

    // 打开地图
    $('.swiper1 .icon-plus').on('touchend', function() {
        var point = $(this).prev().find('.label-address').text();
        $('.modal-map').fadeIn();
        local.search(point);
    });
    // 关闭地图
    $('.modal-map .btn-close').on('touchend', function() {
        $('.modal-map').fadeOut(300);
    });
});