jQuery(function($) {
	var startx, starty; // 起始数值
	// 获得角度
	function getAngle(angx, angy) {
        return Math.atan2(angy, angx) * 180 / Math.PI;
    };
	//根据起点终点返回方向 1向上 2向下 3向左 4向右 0未滑动
    function getDirection(startx, starty, endx, endy) {
        var angx = endx - startx;
        var angy = endy - starty;
        var result = 0;
        //如果滑动距离太短
        if (Math.abs(angx) < 2 && Math.abs(angy) < 2) {
            return result;
        };
        var angle = getAngle(angx, angy);
        if (angle >= -135 && angle <= -45) {
            result = 1;
        } else if (angle > 45 && angle < 135) {
            result = 2;
        } else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
            result = 3;
        } else if (angle >= -45 && angle <= 45) {
            result = 4;
        };
        return result;
    };

	var slider = document.getElementById('box-slider');
	// 接触屏幕
	slider.addEventListener('touchstart', function(e) {
		startx = e.changedTouches[0].pageX;
        starty = e.changedTouches[0].pageY;
	}, false);
	// 滑动屏幕
	slider.addEventListener('touchmove', function(e) {
		var endx, endy;
		endx = e.changedTouches[0].pageX;
		endy = e.changedTouches[0].pageY;
		var dir = getDirection(startx, starty, endx, endy);
		if(dir == 3) {
			// 向左滑动
			// console.log('向左移动距离: ' + (startx - endx));
			if(Math.abs(endx - startx)/1.5 > 1) {
				if($('.box-slider img').data('index') < 36) {
					var num = $('.box-slider img').data('index') + 1;
					$('.box-slider img').data('index', num);
					$('.box-slider img').attr('src', '../3d/180/bg/JLSJLGJL/SJL/0_' + num + '.png');
				};
			};
		} else if(dir == 4) {
			// 向右滑动
			// console.log('向右移动距离: ' + (endx - startx));
			if(Math.abs(endx - startx)/1.5 > 1) {
				if($('.box-slider img').data('index') > 0) {
					var num = $('.box-slider img').data('index') - 1;
					$('.box-slider img').data('index', num);
					$('.box-slider img').attr('src', '../3d/180/bg/JLSJLGJL/SJL/0_' + num + '.png');
				};
			};
		};
		startx = endx;
	}, false);
	// slide.addEventListener('touchend', function(e) {
	// 	var endx, endy;
	// 	endx = e.changedTouches[0].pageX;
	// 	endy = e.changedTouches[0].pageY;
	// 	var dir = getDirection(startx, starty, endx, endy);
	// 	console.log(direction);
	// }, false);





	return;
	setImg()
	function setImg() {
    $(".screenInner .mouseMove").children().remove();
    $(".screenInner .silderBlock").css({
        visibility: "visible"
    });
    var _length = $(".silder .imgWrap").length;
    for (var i = 0; i < _length; i++) {
        var src = $(".silder .imgWrap").eq(i).data("src");
        var oImg = document.createElement('img');
        oImg.src = src;
        if ($(".silder .imgWrap").eq(i).children().length == 0) {
            $(".silder .imgWrap").eq(i).append(oImg);
        }
    }

    imgLoad($(".silder .imgWrap").eq(_length - 1).find("img").get(0), function() {

        $(".screenInner .rotate").css({
            visibility: "visible"
        });
        mobilemoverSilder();
        moverSilder();
        mouseMove();
        mobileMove();
        silderEffe();

    })

}
function imgLoad(img2, callback) {

    var timer = setInterval(function() {
        if (img2.complete) {
            callback(img2)
            clearInterval(timer)
        }

    }, 50)
}

function moverSilder() {
    var silderLength = $(".silder").eq(0).find(".imgWrap").length;

    //起点 left: 101px;    top: 110px;
    //终点left: 0px;    top: 13px;
    //差值101         97

    /* 2018-9-27 */
    //起点 left: 77px;    top: 60px;
    //终点left: -1px;    top: 8px;
    //差值78         52
    var _left = parseFloat($(".screenInner .rotateImg").css("left"));
    var _top = parseFloat($(".screenInner .rotateImg").css("top"));
    var _x = 0
      , _y = 0;
    $(".screenInner .rotateImg").mousedown(function(e) {
        _left = parseFloat($(".screenInner .rotateImg").css("left"));
        _top = parseFloat($(".screenInner .rotateImg").css("top"));
        _x = e.pageX;
        $(".screenInner .rotate").mousemove(function(e) {
            var _dx = e.pageX - _x;
            var _willD = _left + _dx;
            if (_willD >= -1 && _willD < 77) {
                // var _dY=_top+_dx/101*97;
                // var _dY=97*Math.sin(2*Math.PI/404*_willD-6)-13;
                var Percentage = parseInt(Math.abs(77 - _willD) / 77 * silderLength);
                $(".silder").trigger("slideTo", Percentage);
                // var _dY=125*Math.sin(2*Math.PI/420*_willD-6)-13;
                // var _dY=125*Math.sin(2*Math.PI/380*_willD-6)-13;
                var _dY = Math.sqrt(190 - Math.pow(_willD, 2) + 115 * _willD) - 5;

                $(".screenInner .rotateImg").css({
                    "left": _willD + "px"
                });
                $(".screenInner .rotateImg").css({
                    "top": _dY + "px"
                });
            }
        })
    })
    $("body").mouseup(function() {
        $(".screenInner .rotate").unbind();
    })

}

function mobilemoverSilder() {
    var isSupportTouch = "ontouchend"in document ? true : false;
    if (!isSupportTouch) {
        return false;
    }
    var silderLength = $(".silder").eq(0).find(".imgWrap").length;
    //起点 left: 101px;    top: 110px;
    //终点left: 0px;    top: 13px;
    //差值101         97
    var _left = parseFloat($(".screenInner .rotateImg").css("left"));
    var _top = parseFloat($(".screenInner .rotateImg").css("top"));
    var _x = 0
      , _y = 0;

    var slideRight = document.getElementsByClassName("rotateImg")[0];

    slideRight.addEventListener('touchstart', function(event) {
        _left = parseFloat($(".screenInner .rotateImg").css("left"));
        _top = parseFloat($(".screenInner .rotateImg").css("top"));
        _x = event.changedTouches[0].pageX;

        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    }, false);
    slideRight.addEventListener('touchmove', function(event) {
        var _dx = event.changedTouches[0].pageX - _x;
        var _willD = _left + _dx;
        if (_willD >= -1 && _willD < 101) {
            // var _dY=_top+_dx/101*97;
            // var _dY=97*Math.sin(2*Math.PI/404*_willD-6)-13;
            var Percentage = parseInt(Math.abs(101 - _willD) / 101 * silderLength);
            $(".silder").trigger("slideTo", Percentage);
            var _dY = Math.sqrt(203 - Math.pow(_willD, 2) + 202 * _willD) + 8;
            // var _dY=110-Math.sqrt(10403-Math.pow(_willD,2)-2*_willD);
            $(".screenInner .rotateImg").css({
                "left": _willD + "px"
            });
            $(".screenInner .rotateImg").css({
                "top": _dY + "px"
            });
        }

        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    }, false);

}

//鼠标滑动翻转
function mouseMove() {
    var _x = 0;
    var silderLength = $(".silder").eq(0).find(".imgWrap").length;
    var Percentage = 0;
    var _width = $(".mouseMove").width() * 8;
    $(".mouseMove").mousedown(function(e) {
        _x = e.pageX;
        $(".mouseMove").mousemove(function(e) {
            var _dx = _x - e.pageX;

            var _willPer = Percentage + _dx / _width;

            if (_willPer > 0 && _willPer < 1) {
                Percentage = _willPer;
                $(".silder").trigger("slideTo", parseInt(Percentage * silderLength));
            }

        })
    })
    $("body").mouseup(function() {
        $(".mouseMove").unbind("mousemove");
    })
}

function mobileMove() {
    var isSupportTouch = "ontouchend"in document ? true : false;
    if (!isSupportTouch) {
        return false;
    }
    var slideRight = document.getElementsByClassName("mouseMove")[0];
    var _x = 0;
    var silderLength = $(".silder").eq(0).find(".imgWrap").length;
    var Percentage = 0;
    var _width = $(".mouseMove").width() * 8;

    slideRight.addEventListener('touchstart', function(event) {
        _x = event.changedTouches[0].pageX;

        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    }, false);
    slideRight.addEventListener('touchmove', function(event) {
        var _dx = _x - event.changedTouches[0].pageX;
        var _willPer = Percentage + _dx / _width;

        if (_willPer > 0 && _willPer < 1) {
            Percentage = _willPer;
            $(".silder").trigger("slideTo", parseInt(Percentage * silderLength));
        }

        if (event.stopPropagation) {
            event.stopPropagation();
        } else {
            event.cancelBubble = true;
        }
    }, false);

}
// 下载
function down2() {
    $(".screenInner .hasHid .comInenr").on("click", function() {

        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this).siblings(".hid").slideUp();
        } else {
            $(this).addClass("active");
            $(this).siblings(".hid").slideDown();
        }

    })
}
//silder切换
function silderEffe() {
    $(".screenInner .color").click(function() {
        $(".silderBlock").css({
            display: "none"
        });
        $(".silderBlock").eq($(this).index() - 1).css({
            display: "block"
        });
        $('.i-link').find('a').eq($(this).index() - 1).removeClass('hide').siblings().addClass('hide');
        console.log($(this).index())
        // $('.silder').trigger("destory");
        silder();
    })
}
//silder
function silder() {

    $('.silder').carouFredSel({
        'responsive': true,
        items: {
            visible: 1
        },
        'scroll': {
            'duration': 10,
            'items': 1,
            'fx': 'crossfade',

        },
        // 'auto': {'duration': 800},
        'auto': false
    });
    $('.silder').trigger("slideTo", 18);
}

function index_gd_home(a, b, w, h) {
    var $window = $(b);
    var $windowWidth = function() {
        return $window.width();
    };
    var $windowHeight = function() {
        return $window.height();
    };
    $window.fssResize(function() {
        var width = $windowWidth();
        var height = $windowHeight();

        $(b).each(function() {
            $this = $(this);
            $this.height(height).width(width);
            $this.triggerHandler("configuration", {
                height: height,
                width: width,
                items: {
                    width: width
                }
            });
            $this.triggerHandler("updateSizes");
        });

    });

    $(a).each(function() {
        $(this).fullscreenstage({
            'width': w,
            'height': h
        });
    });

    $(window).fssResize();

}


});