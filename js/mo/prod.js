jQuery(function($) {
    // 产品搜索
    $('.prod-search').on('touchend', function() {
    	console.log($('.input-search').val());
    	if($('.input-search').val() == '') {
    		alert('请输入关键字');
    	};
    });
    $('.btn-search').on('touchend', function() {
    	console.log($('.input-search').val());
    	if($('.input-search').val() == '') {
    		alert('请输入关键字');
    	};
    });

    // 关键字搜索
    $('.box-keywords span').on('touchend', function() {
    	$('.input-search').val($(this).text());
    });

    // 选型号
    $('.type-item').on('touchend', function() {
    	if($(this).hasClass('opacity')) {
    		$(this).removeClass('opacity');
    		if(!$(this).siblings().hasClass('opacity')) {
    			$(this).siblings().addClass('opacity');
    		};
    	} else {
    		$(this).addClass('opacity');
    	}
    });
    // 选制冷/热
    $('.areatype-item').on('touchend', function() {
    	if($(this).hasClass('opacity')) {
    		$(this).removeClass('opacity');
    		if(!$(this).siblings().hasClass('opacity')) {
    			$(this).siblings().addClass('opacity');
    		};
    	} else {
    		$(this).addClass('opacity');
    	}
    });

    // swiper
    var swiper3 = new Swiper('.swiper3', {
		autoplayDisableOnInteraction : false,
		loop: true,
		prevButton:'.swiper-button-prev',
		nextButton:'.swiper-button-next',
		pagination: '.swiper-pagination3'
	});
});