jQuery(function($) {
	// sec0 swiper
	var swiper0 = new Swiper('.swiper0', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -9, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 9, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination0'
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec0 .box-swiper'));
		$.fadeUpAn($('.sec1 .box-title1-1'));
		$.fadeUpAn($('.sec1 .year1994'));
		$.fadeUpAn($('.sec1 .year2005'));
		$.fadeUpAn($('.sec1 .year2014'));
		$.fadeUpAn($('.sec1 .year2016'));
		$.fadeUpAn($('.sec1 .year2019'));
		$.fadeUpAn($('.sec1 .box-title1-2'));
		$.fadeUpAn($('.sec1 .content1-2'));
		$.fadeUpAn($('.sec1 .subtitle1-3'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2-1'));
		$.fadeUpAn($('.sec2 .content2-2'));
		$.fadeUpAn($('.sec2 .content2-3'));
		$.fadeUpAn($('.sec3 .othertitle'));
		$.fadeUpAn($('.sec3 .box-other'));
		$.fadeUpAn($('.sec3 .divider'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec0 .box-swiper'));
		$.fadeUpAn($('.sec1 .box-title1-1'));
		$.fadeUpAn($('.sec1 .year1994'));
		$.fadeUpAn($('.sec1 .year2005'));
		$.fadeUpAn($('.sec1 .year2014'));
		$.fadeUpAn($('.sec1 .year2016'));
		$.fadeUpAn($('.sec1 .year2019'));
		$.fadeUpAn($('.sec1 .box-title1-2'));
		$.fadeUpAn($('.sec1 .content1-2'));
		$.fadeUpAn($('.sec1 .subtitle1-3'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2-1'));
		$.fadeUpAn($('.sec2 .content2-2'));
		$.fadeUpAn($('.sec2 .content2-3'));
		$.fadeUpAn($('.sec3 .othertitle'));
		$.fadeUpAn($('.sec3 .box-other'));
		$.fadeUpAn($('.sec3 .divider'));
	});

	// hover
	$('.btn-check').on('touchstart', function() {
		$(this).attr('src', '../img/mo/btn-check-active.png');
	});
});