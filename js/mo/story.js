jQuery(function($) {
	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-swiper'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		$.fadeUpAn($('.sec3 .othertitle'));
		$.fadeUpAn($('.sec3 .box-other'));
		$.fadeUpAn($('.sec3 .divider'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-swiper'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-swiper'));
		$.fadeUpAn($('.sec3 .othertitle'));
		$.fadeUpAn($('.sec3 .box-other'));
		$.fadeUpAn($('.sec3 .divider'));
	});

	// sec1 swiper
	var swiper1 = new Swiper('.swiper1', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -9, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 9, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination1'
	});

	// sec2 swiper
	var swiper2 = new Swiper('.swiper2', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -9, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 9, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination2'
	});
});