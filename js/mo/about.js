jQuery(function($) {
	// swiper1
	var swiper1 = new Swiper('.swiper1', {
		loop: true,
		prevButton:'.swiper-button-prev1',
		nextButton:'.swiper-button-next1',
		pagination: '.swiper-pagination1'
	});

	// swiper2
	var swiper2 = new Swiper('.swiper2', {
		initialSlide: 0,
		parallax: true,
		onSlideChangeEnd: function(swiper){
			// console.log(swiper.activeIndex);
			$($('.btns').children()[swiper.activeIndex]).addClass('active')
			$($('.btns').children()[swiper.activeIndex]).siblings().removeClass('active');
	    }
	});

	// swiper4
	var swiper4 = new Swiper('.swiper4', {
		loop: true,
		prevButton:'.swiper-button-prev4',
		nextButton:'.swiper-button-next4',
		pagination: '.swiper-pagination4'
	});

	// 社会责任
	$('.section4 .slide-title').on('touchend', function() {
		$(this).next().stop(true, true).slideToggle();
		$(this).find('.divider2').toggleClass('hide');
		if($(this).find('.divider2').hasClass('hide')) {
			// 当前展开 其他折叠
			$(this).parent().siblings().each(function() {
				if($(this).find('.divider2').hasClass('hide')) {
					$(this).find('.slide-content').stop(true, true).slideUp();
					$(this).find('.divider2').removeClass('hide');
				};
			});
		};
	});
});