jQuery(function($) {
	// sec1 swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		initialSlide: 1,
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -49, /* 值越小两边的slide越靠近中间的slide */
            depth: 130,  /* 值越大两边的slide越小 */
            modifier: 4, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		onSlideChangeEnd: function(swiper) {
			// console.log(swiper.realIndex);
			// 改变色块
			$('.sec1 .color-item' + (swiper.realIndex + 1)).addClass('active').siblings().removeClass('active');
			// 改变360链接
			switch(swiper.realIndex) {
				case 0:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JLSJLGJL/JL/index.html');
					break;
				case 1:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JLSJLGJL/SJL/index.html');
					break;
				case 2:
					$('.sec1 .btn-360').attr('href', '../3d/360/bg/JLSJLGJL/GJL/index.html');
					break;
				default:
					//
			};
	    }
	});

	// 180 check
	$.prod180An('box-jl', $('#box-jl'), '../3d/180/bg/JLSJLGJL/JL/0_', 1, 37);
	$.prod180An('box-sjl', $('#box-sjl'), '../3d/180/bg/JLSJLGJL/SJL/0_', 0, 36);
	$.prod180An('box-gjl', $('#box-gjl'), '../3d/180/bg/JLSJLGJL/GJL/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(swiper1.realIndex) {
			case 0:
				$('.modal-jl').fadeIn(300);
				break;
			case 1:
				$('.modal-sjl').fadeIn(300);
				break;
			case 2:
				$('.modal-gjl').fadeIn(300);
				break;
			default:
				//
		};
	});

	// sec1 download
	$('.sec1 .box-download img').on('click', function() {
		$('.modal-download').fadeIn(300);
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sect2 content
	$('.sec2 .btn-check-plus').on('click', function() {
		$('.content-item1').removeClass('show');
		$('.content-item2').addClass('show');
	});
	$('.sec2 .content-item2').on('click', function() {
		$(this).removeClass('show');
		$('.content-item1').addClass('show');
	});

	// 自动播放
	function playerFun(el) {
        if (window.WeixinJSBridge) {
            WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                el.get(0).play();
            }, false);
        } else {
            document.addEventListener("WeixinJSBridgeReady", function() {
                WeixinJSBridge.invoke('getNetworkType', {}, function(e) {
                    el.get(0).play();
                });
            }, false);
        }
        setTimeout(function() {
            el.get(0).play();
        }, 100);
    };

	// sec3 player
	var player3;
	var getPlayer = setInterval(function() {
		player3 = $('#ifr3').contents().find('#player3');
		if(player3 != undefined && player3 != null) {
			if(player3.length > 0) {
				clearInterval(getPlayer);
				$(window).on('scroll', function() {
					if($.visionAn($('#ifr3'))) {
						if(player3[0].paused) {
							playerFun(player3);
						};
					} else {
						if(!player3[0].paused) {
							player3[0].pause();
						};
					};
				});
			};
		};
	}, 300);
	
	// sec4
	var swiperbg = new Swiper('.sec4 .swiperbg', {
		noSwiping: true,
		loop: true,
		effect: 'fade'
	});
	var swiperprod = new Swiper('.sec4 .swiperprod', {
		loop: true,
		pagination: '.swiper-paginationprod',
		onSlideChangeEnd: function(swiper) {
			$('.sec4 .title4-2').attr('src', '../img/mo/seriesbg/jl/title4-2-' + (swiper.realIndex + 1) + '.png');
			switch(swiper.realIndex) {
				case 0:
					$('.sec4 .divider').addClass('color1').removeClass('color2').removeClass('color3');
					$('.sec4 .box-series').addClass('color1').removeClass('color2').removeClass('color3');
					$('.sec4 .box-series .series-item1').addClass('active').siblings().removeClass('active');
					break;
				case 1:
					$('.sec4 .divider').addClass('color2').removeClass('color1').removeClass('color3');
					$('.sec4 .box-series').addClass('color2').removeClass('color1').removeClass('color3');
					$('.sec4 .box-series .series-item2').addClass('active').siblings().removeClass('active');
					break;
				case 2:
					$('.sec4 .divider').addClass('color3').removeClass('color1').removeClass('color2');
					$('.sec4 .box-series').addClass('color3').removeClass('color1').removeClass('color2');
					$('.sec4 .box-series .series-item3').addClass('active').siblings().removeClass('active');
					break;
				default:
					//
			};
			$('.sec4 .subtitle4-2').attr('src', '../img/mo/seriesbg/jl/subtitle4-2-' + (swiper.realIndex + 1) + '.png');
		}
	});
	var swiperinfo = new Swiper('.swiperinfo', {
		noSwiping: true,
		loop: true,
		// spaceBetween: -130
	});

	// sec6
	function opacityAn(el1, el2) {
		if(el1.offset().top >= $(window).scrollTop() && el1.offset().top < ($(window).scrollTop() + $(window).height() * 0.75)) {
			el2.stop(true, true).animate({opacity: 0.5}, 800);
		} else {
			el2.stop(true, true).animate({opacity: 1}, 800);
		};
	}
	opacityAn($('.sec6 .box-cards'), $('.sec6 .content'));
	opacityAn($('.sec7 .box-cards'), $('.sec7 .content'));
	$(window).on('scroll', function() {
		opacityAn($('.sec6 .box-cards'), $('.sec6 .content'));
		opacityAn($('.sec7 .box-cards'), $('.sec7 .content'));
		windAn();
	});

	// sec7 
	function windAn() {
		if($('.sec7 .box-effect').offset().top >= $(window).scrollTop() && $('.sec7 .box-effect').offset().top < ($(window).scrollTop() + $(window).height() * 0.75)) {
			$('.sec7 .box-wind').animate({height: '293.97%'}, 3000);
		};
	}
	windAn();

	// sec9
	var swiper9 = new Swiper('.swiper9', {
		loop: true,
		pagination: '.swiper-pagination9',
		effect: 'fade'
	});

	// swiper control
	swiperprod.params.control = [swiperbg, swiperinfo, swiper9];
	swiper9.params.control = [swiperprod];

	// sec10
	var swiper10 = new Swiper('.swiper10', {
		loop: true,
		slidesPerView: 2,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -4, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 9, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination10',
	});

});