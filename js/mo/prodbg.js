jQuery(function($) {
    // swiper banner
    var swiper0 = new Swiper('.swiper0', {
        loop: true,
        prevButton:'.swiper-button-prev',
        nextButton:'.swiper-button-next'
    });

    // 筛选
    $('.section1 .btn-screen').on('touchend', function() {
        $('.modal-screen').fadeIn(300);
    });

    // modal
    $('.modal .select-item').on('touchend', function() {
        if($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        } else {
            $(this).addClass('selected');
        };
    });

    // reset
    $('.modal .btn-reset').on('touchend', function() {
        $('.modal .select-item').removeClass('selected');
    });

    // submit
    $('.modal .btn-submit').on('touchend', function() {
        $('.modal-screen').fadeOut(300);
        console.log($('.box-function .selected').find('span').text());
        console.log($('.box-effect .selected').find('span').text());
        console.log($('.box-type .selected').find('span').text());
        console.log($('.box-area .selected').find('span').text());
    });
});