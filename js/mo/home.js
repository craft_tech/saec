jQuery(function($) {
	// sec0 swiper
	var swiper0 = new Swiper('.sec0 .swiper0', {
		loop: true,
		autoplayDisableOnInteraction : false,
		prevButton:'.swiper-button-prev',
		nextButton:'.swiper-button-next'
	});

	// sec0 video
	var player0;
	var getPlayer = setInterval(function() {
		player0 = $('#ifr0').contents().find('#player0');
		if(player0 != undefined && player0 != null) {
			if(player0.length > 0) {
				clearInterval(getPlayer);
			};
		};
	}, 300);
	$('.sec0 .btn-play').on('click', function() {
		$('.modal-home').fadeIn(300);
	});
	// close video
	$('.modal-home .btn-close').on('click', function() {
		if(player0[0].paused) {
			//
		} else {
			player0[0].pause();
		};
		$('.modal-home').fadeOut(300);
	});

	// 淡入动画
	setTimeout(function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-other'));
		$.fadeUpAn($('.sec1 .box-zykt'));
		$.fadeUpAn($('.sec2 .box-title2-1'));
		$.fadeUpAn($('.sec2 .box-video'));
		$.fadeUpAn($('.sec2 .box-title2-2'));
		$.fadeUpAn($('.sec2 .btn-more-l'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .btn-more-l'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .btn-more-l'));
		$.fadeUpAn($('.sec5 .othertitle'));
		$.fadeUpAn($('.sec5 .box-other'));
		$.fadeUpAn($('.sec5 .divider'));
	}, 300);
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-other'));
		$.fadeUpAn($('.sec1 .box-zykt'));
		$.fadeUpAn($('.sec2 .box-title2-1'));
		$.fadeUpAn($('.sec2 .box-video'));
		$.fadeUpAn($('.sec2 .box-title2-2'));
		$.fadeUpAn($('.sec2 .btn-more-l'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .btn-more-l'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .btn-more-l'));
		$.fadeUpAn($('.sec5 .othertitle'));
		$.fadeUpAn($('.sec5 .box-other'));
		$.fadeUpAn($('.sec5 .divider'));
	});

	// hover
	$('.sec0 .btn-play').hover(function() {
		$(this).attr('src', '../img/mo/home/btn-play-active.png');
	}, function() {
		$(this).attr('src', '../img/mo/home/btn-play.png');
	});
	$('.btn-more-s').on('touchstart', function() {
		$(this).attr('src', '../img/mo/btn-more-s-active.png');
	});
	$('.box-zykt img').on('touchstart', function() {
		$(this).attr('src', '../img/mo/home/btn-zykt-active.png');
	});
	$('.btn-more-l').on('touchstart', function() {
		$(this).attr('src', '../img/mo/btn-more-l-active.png');
	});
});