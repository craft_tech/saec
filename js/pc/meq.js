jQuery(function($) {
	// 淡入动画
	$.fadeUpAn($('.sec1 .box-title1'));
	$.fadeUpAn($('.sec1 .divider'));
	$.fadeUpAn($('.sec1 .box-title1-2'));
	$.fadeUpAn($('.sec1 .content1'));
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec3 .box-title3'));
	$.fadeUpAn($('.sec4 .box-title4'));
	$.fadeUpAn($('.sec5 .box-title5'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .divider'));
		$.fadeUpAn($('.sec1 .box-title1-2'));
		$.fadeUpAn($('.sec1 .content1'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec5 .box-title5'));
	});

	// scroll to 1
	$('.sec1 .cont1').on('click', function() {
		$('html, body').animate({
			scrollTop: $(document).height() * 0.226,
		}, 200);
	}); 
	// scroll to 2
	$('.sec1 .cont2').on('click', function() {
		$('html, body').animate({
			scrollTop: $(document).height() * 0.355,
		}, 400);
	});
	// scroll to 3
	$('.sec1 .cont3').on('click', function() {
		$('html, body').animate({
			scrollTop: $(document).height() * 0.484,
		}, 600);
	});
	// scroll to 4
	$('.sec1 .cont4').on('click', function() {
		$('html, body').animate({
			scrollTop: $(document).height() * 0.602,
		}, 800);
	});
});