jQuery(function($) {

    // result变化
    function setResult() {
        if($(window).width() > 750) {
            var h = ($(window).width() * 39.4) / 1920;
            $('.sec1 .box-result').css({'height': h + 'rem'});
        } else {
            $('.sec1 .box-result').css({'height': '16rem'});
        };
    };
    function setResult2() {
        if($(window).width() > 750) {
            var h = ($(window).width() * 58.6) / 1920;
            $('.sec2 .box-result').css({'height': h + 'rem'});
        } else {
            $('.sec2 .box-result').css({'height': '16rem'});
        };
    };
    setResult();
    setResult2();
    $(window).on('resize', function() {
        if($(window).width() > 750) {
            setResult();
            setResult2();
        };
    });

    // 折叠
    $('.sec1 .res-title').on('click', function() {
    	$(this).next().stop(true, true).slideToggle();
    });

    // 可选城市 需调接口
    var provinceArr = [
    	{
    		id: 0,
    		value: '上海',
    		city: ['上海市']
    	},
    	{
    		id: 1,
    		value: '浙江',
    		city: ['杭州', '嘉兴', '金华', '义乌']
    	}
    ];

    // fill province
    if($('.sec1 .province .default').siblings().length == 0) {
    	var len = provinceArr.length;
    	for(var i = 0; i < len; i++) {
    		var $option = $('<option data-id="' + provinceArr[i].id + '" value="' + provinceArr[i].value + '">' + provinceArr[i].value + '</option>');
    		$('.sec1 .province').append($option);
    	};
    };
    // fill city
    $('.sec1 .province').on('input propertychange', function() {
    	if($('.sec1 .city .default').siblings().length > 0) {
    		$('.sec1 .city .default').siblings().remove();
    	};
    	if($(this).val() != '') {
    		var id = $(this).find('option:selected').data('id');
			var len = provinceArr[id].city.length;
			for(var i = 0; i < len; i++) {
				var $option = $('<option value="' + provinceArr[id].city[i] + '">' + provinceArr[id].city[i] + '</option>');
				$('.sec1 .city').append($option);
			};
    	};
	});

    // 百度地图API功能
	var map = new BMap.Map("shopmap");    // 创建Map实例
	map.centerAndZoom('上海', 12);  // 初始化地图,设置中心点坐标和地图级别
    var local = new BMap.LocalSearch(map, {
        renderOptions:{
            map: map,
            // autoViewport: true 
        }
    });

	// 定位
	$('.sec1 .btn-pos').on('click', function() {
		if($('.sec1 .province').val() == '') {
			alert('请选择省份');
		} else {
			if($('.sec1 .city').val() == '') {
				alert('请选择城市');
			} else {
				map.centerAndZoom($('.sec1 .city').val(), 12);
			};
		};
	});

    // 定位
    $('.sec1 .btn-plus').on('click', function() {
        var point = $(this).siblings('.res-title').find('.res-name span').text();
        setTimeout(function() {
            local.search(point);
            map.panBy(-(($(window).width() * 450) / 1920), ($(window).width() * 500) / 1920);
        }, 300);
    });
});