jQuery(function($) {
	// sec1
	// download
	$('.box-download').on('click', function() {
		console.log('download');
		$('.modal-download').fadeIn(300);
	});

	// 切换产品 初始值是2，中间的颜色
	$('.sec1 .color-item' + $('.sec1 .box-prod img').data('index')).addClass('active');
	$('.sec1 .btn-360').attr('href', '3d/360/bg/JLSJLGJL/SJL/index.html');
	$('.sec1 .color-item').on('click', function() {
		$(this).addClass('active').siblings().removeClass('active');
		$('.sec1 .box-prod img').data('index', $(this).data('color'));
		$('.sec1 .box-prod img').attr('src', 'img/pc/seriesbg/jl/prod1-' + $(this).data('color') + '.png');
		// 改变360链接
		switch(parseInt($(this).data('color'))) {
			case 1:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JLSJLGJL/JL/index.html');
				break;
			case 2:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JLSJLGJL/SJL/index.html');
				break;
			case 3:
				$('.sec1 .btn-360').attr('href', '3d/360/bg/JLSJLGJL/GJL/index.html');
				break;
			default:
				//
		};
	});
	// 180 check
	$.prod180An('box-jl', $('#box-jl'), '3d/180/bg/JLSJLGJL/JL/0_', 1, 37);
	$.prod180An('box-sjl', $('#box-sjl'), '3d/180/bg/JLSJLGJL/SJL/0_', 0, 36);
	$.prod180An('box-gjl', $('#box-gjl'), '3d/180/bg/JLSJLGJL/GJL/0_', 0, 36);
	// 180 modal
	$('.sec1 .btn-180').on('click', function() {
		switch(parseInt($('.sec1 .box-prod img').data('index'))) {
			case 1:
				$('.modal-jl').fadeIn(300);
				break;
			case 2:
				$('.modal-sjl').fadeIn(300);
				break;
			case 3:
				$('.modal-gjl').fadeIn(300);
				break;
			default:
				//
		};
	});

	// close modal
	$('.modal .btn-close').on('click', function() {
		$('.modal').fadeOut(300);
	});

	// sect2 
	// 切换
	$('.sec2 .btn-check-plus').on('click', function() {
		$('.content-item2').removeClass('show');
		$('.content-item3').addClass('show');
	});
	$('.sec2 .content-item3').on('click', function() {
		$(this).removeClass('show');
		$('.content-item2').addClass('show');
	});

	// sec3
	// player
	$(window).on('scroll', function() {
		if($('#player3').offset().top * 0.8 <= $(window).scrollTop() && $('#player3').offset().top < ($(window).scrollTop() + $(window).height() * 0.75)) {
			if($('#player3')[0].paused) {
				$('#player3')[0].play();
			};
		} else {
			if(!$('#player3')[0].paused) {
				$('#player3')[0].pause();
			};
		};
	});
	



	// // 淡入动画
	// $.fadeUpAn($('.sec1 .box-title1'));
	// $.fadeUpAn($('.sec1 .box-other'));
	// $.fadeUpAn($('.sec1 .box-zykt'));
	// $.fadeUpAn($('.sec2 .box-title2'));
	// $.fadeUpAn($('.sec2 .box-video'));
	// $.fadeUpAn($('.sec2 .box-title2-2'));
	// $.fadeUpAn($('.sec2 .btn-more-l'));
	// $.fadeUpAn($('.sec3 .box-title3'));
	// $.fadeUpAn($('.sec3 .btn-more-l'));
	// $.fadeUpAn($('.sec4 .box-title4'));
	// $.fadeUpAn($('.sec4 .btn-more-l'));
	// // scroll
	// $(window).on('scroll', function() {
	// 	$.fadeUpAn($('.sec1 .box-title1'));
	// 	$.fadeUpAn($('.sec1 .box-other'));
	// 	$.fadeUpAn($('.sec1 .box-zykt'));
	// 	$.fadeUpAn($('.sec2 .box-title2'));
	// 	$.fadeUpAn($('.sec2 .box-video'));
	// 	$.fadeUpAn($('.sec2 .box-title2-2'));
	// 	$.fadeUpAn($('.sec2 .btn-more-l'));
	// 	$.fadeUpAn($('.sec3 .box-title3'));
	// 	$.fadeUpAn($('.sec3 .btn-more-l'));
	// 	$.fadeUpAn($('.sec4 .box-title4'));
	// 	$.fadeUpAn($('.sec4 .btn-more-l'));
	// });

	// hover
	$('.sec0 .btn-play').hover(function() {
		$(this).attr('src', 'img/pc/index/btn-play-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/index/btn-play.png');
	});
	$('.btn-more-s').hover(function() {
		$(this).attr('src', 'img/pc/btn-more-s-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/btn-more-s.png');
	});
	$('.box-zykt img').hover(function() {
		$(this).attr('src', 'img/pc/index/btn-zykt-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/index/btn-zykt.png');
	});
	$('.btn-more-l').hover(function() {
		$(this).attr('src', 'img/pc/btn-more-l-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/btn-more-l.png');
	});
});