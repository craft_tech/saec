jQuery(function($) {
    // sec1 
    // 筛选折叠
    $('aside .title').on('click', function() {
        $(this).next().stop(true, true).slideToggle();
        $(this).find('.icon-slide').toggleClass('hide');
        $(this).find('.icon-slide-down').toggleClass('hide');
    });
    // 筛选
    $('aside .btn-screen').on('click', function() {
        console.log($('aside .item-function').find('input:checked').length);
        console.log($('aside .item-effect').find('input:checked').length);
    });
    // 重置
    $('aside .btn-reset').on('click', function() {
        $('aside').find('input:checked').prop('checked', false);
    });

    // 调接口
    $.ajax({
        type: 'get',
        url: 'http://10.152.153.165:3344/service/api/Product/GetChannelProducts',
        dataType: 'json',
        // contentType: 'application/json',
        data: {
            cnl: 4,
            page: 1,
            func: '',
            consume: '',
            feature: '',
            area: ''
        },
        // async: false,
        cache: false,
        success: function(data) {
            console.log(data);
        },
        error: function() {
            //
            alert('网络错误，请刷新重试');
        },
        complete: function(data) {
            // 调取完毕
            // $('.modal-loading').fadeOut('fast');
        }
    });
});