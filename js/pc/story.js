jQuery(function($) {
	// sec1
	var swiper1 = new Swiper('.swiper1', {
		loop: true,
		slidesPerView: 4,
		centeredSlides : true,
		effect:"coverflow",
		coverflow: {
            rotate: 0,
            stretch: -9, /* 值越小两边的slide越靠近中间的slide */
            depth: 0,  /* 值越大两边的slide越小 */
            modifier: 5, /* 值越小两边的slide越小 */
            slideShadows: false
        },
		pagination: '.swiper-pagination1',
		paginationType : 'progress'
	});

	// 淡入动画
	$.fadeUpAn($('.sec0 .box-title0'));
	$.fadeUpAn($('.sec1 .box-title1'));
	$.fadeUpAn($('.sec1 .box-swiper'));
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .content2'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-swiper'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2'));
	});
});