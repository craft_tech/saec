jQuery(function($) {
	// 淡入动画
	$.fadeUpAn($('.sec0 .box-title0'));
	$.fadeUpAn($('.sec0 .content0'));
	$.fadeUpAn($('.sec1 .box-title1'));
	$.fadeUpAn($('.sec1 .year1994'));
	$.fadeUpAn($('.sec1 .year2005'));
	$.fadeUpAn($('.sec1 .year2014'));
	$.fadeUpAn($('.sec1 .year2016'));
	$.fadeUpAn($('.sec1 .year2019'));
	$.fadeUpAn($('.sec1 .box-title1-2'));
	$.fadeUpAn($('.sec1 .content1-2'));
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .content2'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec0 .box-title0'));
		$.fadeUpAn($('.sec0 .content0'));
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .year1994'));
		$.fadeUpAn($('.sec1 .year2005'));
		$.fadeUpAn($('.sec1 .year2014'));
		$.fadeUpAn($('.sec1 .year2016'));
		$.fadeUpAn($('.sec1 .year2019'));
		$.fadeUpAn($('.sec1 .box-title1-2'));
		$.fadeUpAn($('.sec1 .content1-2'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .content2'));
	});

	// hover
	$('.btn-check').hover(function() {
		$(this).attr('src', 'img/pc/moveeye/btn-check-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/moveeye/btn-check.png');
	});
});