jQuery(function($) {
	// sec0
	var swiper0 = new Swiper('.sec0 .swiper0', {
		loop: true,
		prevButton:'.swiper-button-prev',
		nextButton:'.swiper-button-next'
	});
	// player
	var player0 = $('#player0');
	$('.sec0 .btn-play').on('click', function() {
		$('.modal-home').fadeIn(300);
	});
	// close player
	$('.modal-home .btn-close').on('click', function() {
		if(player0[0].paused) {
			//
		} else {
			player0[0].pause();
		};
		$('.modal-home').fadeOut(300);
	});

	// 淡入动画
	$.fadeUpAn($('.sec1 .box-title1'));
	$.fadeUpAn($('.sec1 .box-other'));
	$.fadeUpAn($('.sec1 .box-zykt'));
	$.fadeUpAn($('.sec2 .box-title2'));
	$.fadeUpAn($('.sec2 .box-video'));
	$.fadeUpAn($('.sec2 .box-title2-2'));
	$.fadeUpAn($('.sec2 .btn-more-l'));
	$.fadeUpAn($('.sec3 .box-title3'));
	$.fadeUpAn($('.sec3 .btn-more-l'));
	$.fadeUpAn($('.sec4 .box-title4'));
	$.fadeUpAn($('.sec4 .btn-more-l'));
	// scroll
	$(window).on('scroll', function() {
		$.fadeUpAn($('.sec1 .box-title1'));
		$.fadeUpAn($('.sec1 .box-other'));
		$.fadeUpAn($('.sec1 .box-zykt'));
		$.fadeUpAn($('.sec2 .box-title2'));
		$.fadeUpAn($('.sec2 .box-video'));
		$.fadeUpAn($('.sec2 .box-title2-2'));
		$.fadeUpAn($('.sec2 .btn-more-l'));
		$.fadeUpAn($('.sec3 .box-title3'));
		$.fadeUpAn($('.sec3 .btn-more-l'));
		$.fadeUpAn($('.sec4 .box-title4'));
		$.fadeUpAn($('.sec4 .btn-more-l'));
	});

	// hover
	$('.sec0 .btn-play').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-play-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-play.png');
	});
	$('.btn-more-s').hover(function() {
		$(this).attr('src', 'img/pc/btn-more-s-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/btn-more-s.png');
	});
	$('.box-zykt img').hover(function() {
		$(this).attr('src', 'img/pc/home/btn-zykt-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/home/btn-zykt.png');
	});
	$('.btn-more-l').hover(function() {
		$(this).attr('src', 'img/pc/btn-more-l-active.png');
	}, function() {
		$(this).attr('src', 'img/pc/btn-more-l.png');
	});
});