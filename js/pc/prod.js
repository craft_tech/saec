jQuery(function($) {
    // sec1 
    // 产品搜索
    $('.sec1 .prod-search').on('click', function() {
    	console.log($('.sec1 .input-search').val());
    	if($('.sec1 .input-search').val() == '') {
    		alert('请输入关键字');
    	};
    });
    $('.sec1 .btn-search').on('click', function() {
    	console.log($('.sec1 .input-search').val());
    	if($('.sec1 .input-search').val() == '') {
    		alert('请输入关键字');
    	};
    });

    // 关键字搜索
    $('.sec1 .box-keywords span').on('click', function() {
    	$('.sec1 .input-search').val($(this).text());
    });

    // sec2
    // 选机型
    $('.sec2 .box-area select').prop('disabled', true);
    $('.sec2 .prod-item').on('click', function() {
        if(!$(this).hasClass('show')) {
            $(this).addClass('show');
        };
        if($(this).siblings().hasClass('show')) {
            $(this).siblings().removeClass('show');
        };
    });
    // 选制冷/热面积
    $('.sec2 .area-item').on('click', function() {
        if($('.sec2 .prod-item.show').length == 0) {
            alert('请选择机型');
        } else {
            if(!$(this).hasClass('show')) {
                $(this).addClass('show');
            };
            if($(this).siblings().hasClass('show')) {
                $(this).siblings().removeClass('show');
            };
            $('.sec2 .box-area select').prop('disabled', false);
        };
    });
    // 选面积
    $('.sec2 .box-area select').on('input propertychange', function() {
        if($(this).val() != '') {
            if(!$('.sec2 .box-area').hasClass('show')) {
                $('.sec2 .box-area').addClass('show');
            };
        } else {
            $('.sec2 .box-area').removeClass('show');
        };
    });
    // 确认
    $('.sec2 .btn-submit').on('click', function() {
        if($('.sec2 .prod-item.show').length == 0) {
            alert('请选择机型');
            return;
        };
        if($('.sec2 .area-item.show').length == 0) {
            alert('请选择制冷/热面积');
            return;
        };
        if($('.sec2 .box-area select').val() == '') {
            alert('请选择面积');
            return;
        };
        console.log($('.sec2 .prod-item.show').data('prod'));
        console.log($('.sec2 .area-item.show').data('type'));
        console.log($('.sec2 .box-area select').val());
    });

    // sec3 
    // hover
    $('.sec3 .content div').hover(function() {
        $(this).addClass('shadow');
    }, function() {
        $(this).removeClass('shadow');
    });
});