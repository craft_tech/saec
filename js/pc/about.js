jQuery(function($) {
	// sec1
	// swiper
	var swiper1 = new Swiper('.sec1 .swiper1', {
		loop: true,
		effect: 'fade',
		prevButton:'.swiper-button-prev1',
		nextButton:'.swiper-button-next1',
		pagination: '.swiper-pagination1'
	});

	// sec2
	// swiper
	var swiper2 = new Swiper('.sec2 .swiper2', {
		initialSlide: 0,
		loop: true,
		onSlideChangeEnd: function(swiper){
			$($('.btns').children()[(swiper.activeIndex - 1)]).addClass('active')
			$($('.btns').children()[(swiper.activeIndex - 1)]).siblings().removeClass('active');
	    }
	});

	// sec4
	// swiper
	var swiper4 = new Swiper('.sec4 .swiper4', {
		initialSlide: 1,
		loop: true,
		prevButton:'.swiper-button-prev4',
		nextButton:'.swiper-button-next4',
		pagination: '.swiper-pagination4'
	});

	// 展开
	function slideRight(el) {
		el.find('.divider2').fadeOut(200);
		el.parent().animate({width: '51.6rem'}, 800);
		el.prev().stop(true, true).animate({width: '12.8rem'}, 800);
		el.next().stop(true, true).animate({opacity: '1'}, 800);
	};
	// 收起
	function slideLeft(el) {
		el.find('.divider2').fadeIn(200);
		el.parent().animate({width: '7.6rem'}, 800);
		el.prev().stop(true, true).animate({width: '7.8rem'}, 800);
		el.next().stop(true, true).animate({opacity: '0'}, 800);
	};
	//
	$('.sec4 .content4-2 .plus').on('click', function() {
		if($(this).parent().width() > 200) {
			// 收起
			slideLeft($(this));
		} else {
			// 展开
			slideRight($(this));
			$(this).parents('.slide-item').siblings().each(function() {
				slideLeft($(this).find('.plus'));
			});
		};
	});
});